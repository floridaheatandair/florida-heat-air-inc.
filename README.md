Founded in 1989, Florida Heat & Air, Inc. has remained a family-owned and operated air conditioning company right here in Fort Myers, Florida for three generations! We take a lot of pride in the quality of our work and thoroughly understand our specialty in the HVAC Industry, limiting our work to residential, light commercial and new construction properties.

What We Offer
We offer our customers a wide variety of Air Conditioning product lines, competitive pricing, as well as superior and valuable customer service, which position us as an industry leader here in Southwest Florida. We specialize in new AC installation, Air Conditioning service, and maintenance for Air Conditioners for Residential, Commercial, and New Construction. Florida Heat & Air has not relied on traditional advertising and has been referral based for over 20 years. That is over 20 years of doing business with our outstanding reputation alone.

Our Commitment
Our goal at Florida Heat & Air is to remain a strong, reliable, visible, and major force within the HVAC industry. We will continue to increase our level of excellence to best serve you, our customer! We pride ourselves on our quality of work and customer care, if you are not satisfied then neither are we. You are the most important part of our business and we will work tirelessly to make sure you are properly taken care of.

We understand that when you need help time is of the essence. We offer same-day air conditioning service and always have 24-hour on-call personnel. When you call us for AC service you can rest assured that we will be sending you a certified HVAC technician, not a salesman. Whether it is your first time allowing us to help you or you are a returning customer, we treat each call as the top priority. We are a full-service air conditioning contractor licensed through the state of Florida.

Website : https://flheatair.com/
